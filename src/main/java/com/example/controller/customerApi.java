package com.example.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManagerFactory;

import org.hamcrest.collection.IsEmptyCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.model.Category;
import com.example.model.Customer;
import com.example.model.Order;
import com.example.model.Product;
import com.example.model.customerCrud;
import com.example.model.orderCrud;

import antlr.collections.List;


@Controller
@RequestMapping("/customerApi")
public class customerApi {
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@Autowired
	private customerCrud customercrud;
	
	@Autowired
	private orderCrud orderCrud;
	
	@Autowired
	private com.example.model.productCrud productCrud;
	
	private Product product;
	
	private Order order;
	
	
	private Customer Cust;
	private Customer Custphone;
	
	//public static final String PATH = "/home/waredmag/public_html/uploads";

	public static final int PAGINATION = 10;
	// public static final Map<String,Object> point=new HashMap<>();
	public static final int PAGINATION_GET_HELP = PAGINATION;
	public static final int PAGINATION_GET_HISTORY = PAGINATION;
	
	@RequestMapping("/register")
	public @ResponseBody Map<String , Object> register(Customer customer,
			@RequestBody(required=false) Customer customerBody){
		Map<String , Object> map=new HashMap<>();
		try {
			if (customerBody != null) {
				customer = customerBody;
			}
			
			if(customer.getName()==null || customer.getEmail()==null 
				|| customer.getPassword()==null ||  Integer.valueOf(customer.getPhone())==null)
			{
				
				map.put("status",100);
				return map;
			}
			
			
			//Customer customerPhone;
			Cust=customercrud.findByPhone(customer.getPhone());
			if(Cust != null){
				
				map.put("status", 200);
				return map;
			}
			Cust=customercrud.findByEmail(customer.getEmail());
			
			if(Cust == null ){
				Cust = new Customer();
			Cust.setEmail(customer.getEmail());
			Cust.setName(customer.getName());
			Cust.setCity(customer.getCity());
			Cust.setPassword(customer.getPassword());
			Cust.setPhone(customer.getPhone());
			Cust.setAddress(customer.getAddress());
			
		   customercrud.save(Cust);
			}
			
			else{
				map.put("status", 300);
				return map;
			}
				
			//return map;
			
		
			
		}
		catch ( Exception e){
			
			map.put("status",404);
		}
			
		map.put("status",400);
		return map;
		
		
	}
	
	@RequestMapping("/login")
	public @ResponseBody Map<String , Object> login(Customer customer ,
			@RequestBody(required = false) Customer customerBody){
		
		Map<String , Object> map=new HashMap<>();
		
		try{
			if(customerBody != null){
				customer=customerBody;
			}
			
			if(customer.getEmail()== null || customer.getPassword() == null){
				map.put("status",100);
				return map;
			}
			
			Cust=customercrud.findByEmail(customer.getEmail());
		    if(Cust == null){
		    	map.put("status",200);
		    	return map; 
		    }
			
			if(Cust.getEmail().equals(customer.getEmail()) && Cust.getPassword().equals(customer.getPassword())){
				
				map.put("status", 300);
				return map;
			}
			else{
				map.put("status", 400);
				return map;
			}
			

			
		}catch(Exception e){
			map.put("status",404);
			return map;
		}
		
		
	}
	
	
//	@RequestMapping("/getOrders")
//	public @ResponseBody List getOrder(@RequestBody Customer customer){
//		List list=(List) orderCrud.findByCustomer(1);
//		return list;
//		
//		
//	}
	@RequestMapping("/addOrder")
//	public @ResponseBody Map<String , Object> addOrder(@RequestBody Customer customer ,
//			@RequestBody Order order ) throws ParseException{
	public @ResponseBody Map<String , Object> addOrder() throws ParseException{
		Map<String , Object> map = new HashMap<>();
		//Cust=customercrud.findByEmail(customer.getEmail());
		Cust=customercrud.findByEmail("email@yahoo.com");
		if(Cust == null){
			map.put("status", 200);
			return map;
		}
		Order orderObject = new Order();
		orderObject.setCustomer(Cust);
	//SimpleDateFormat timeDate = new SimpleDateFormat("yyyy-MM-dd");
		
		  Date dNow = new Date( );
//	      SimpleDateFormat ft = 
//	      new SimpleDateFormat ("yyyy.MM.dd");
		//orderObject.setDate((java.sql.Date) dNow);
		  Set<Product> pro=new HashSet<>();
		  product.setName("first produuuuct");
		  product.setPathImage("test image");
		  pro.add(productCrud.findByName("name"));
		  
		  orderObject.setProduct(pro);
  
		orderObject.setTotalCost(1235);
		orderCrud.save(orderObject);
		
		return map;
		
	}
	
	@RequestMapping("/updateCustomer")
	public @ResponseBody Map<String , Object> updateCustomer(@RequestBody Customer customer){
		
		Map<String , Object> map =new HashMap<>();
		Cust=customercrud.findByEmail(customer.getEmail());
		
		try{
			if(Cust != null){
				int phone = Cust.getPhone();
				if(phone != customer.getPhone()){
					Custphone=customercrud.findByPhone(customer.getPhone());
					if(Custphone == null){
						Cust.setEmail(customer.getEmail());
						Cust.setName(customer.getName());
						Cust.setCity(customer.getCity());
						Cust.setPassword(customer.getPassword());
						Cust.setPhone(customer.getPhone());
						Cust.setAddress(customer.getAddress());
						
					   customercrud.save(Cust);
					   
					}
					else{
						map.put("status", 300);
						return map;
					}
				}
			
			
				
				
			}
			
			else{
				map.put("status", 400);
				return map;
			}
			
		}
		catch(Exception e ){
			map.put("status", 404);
			return map;
		}
		map.put("ststus", 100);
		return map;
	}
	
	@RequestMapping("/getCustomer")
	public @ResponseBody Map<String, Object>  getCustomer(@RequestBody Customer customer){
		
		Map<String, Object> map = new HashMap<>();
		Cust = customercrud.findByEmail(customer.getEmail());
		if(Cust == null){
			map.put("status",200);
			return map;
		}
		
		map.put("name",customer.getName());
		map.put("password",customer.getEmail());
		map.put("phone",customer.getPhone());
		map.put("city",customer.getCity());
		map.put("address",customer.getAddress());
		return map;
	} 
	
	
//	@RequestMapping("/allOrders")
//	public @ResponseBody List<Order> allOrders(){
//		List<Order> list=orderCrud.findAll().forEach(list::add);
//		
//		return list;
//	}

}
