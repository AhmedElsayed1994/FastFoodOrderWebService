package com.example.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.model.Admin;
import com.example.model.AdminService;
import com.example.model.Category;
import com.example.model.Customer;
import com.example.model.Offers;
import com.example.model.Order;
import com.example.model.Product;
import com.example.model.ProductService;
import com.example.model.adminCrud;
import com.example.model.categoryCrud;
import com.example.model.offersCrud;
import com.example.model.productCrud;

@Controller
@RequestMapping("/adminApi")
public class adminApi {
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@Autowired
	private offersCrud offercrud;
	
	@Autowired
	private productCrud productCrud;
	
	@Autowired
	private categoryCrud categorycrud;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private adminCrud admincrud;
	

	
	
	private Offers offersObject;
	private Category categoryObject;
	
	private Admin adminObject;
	private Product productObject;
	
	
	@RequestMapping("/login")
	public @ResponseBody Map<String , Object> login(Admin admin ,
			@RequestBody(required = false) Admin adminBody){
		
		Map<String , Object> map=new HashMap<>();
		
		try{
			if(adminBody != null){
				admin=adminBody;
			}
			
			if(admin.getEmail()== null || admin.getPassword() == null){
				map.put("status",100);
				return map;
			}
			
			adminObject=admincrud.findByEmail(admin.getEmail());
		    if(adminObject == null){
		    	map.put("status",200);
		    	return map; 
		    }
			
			if(adminObject.getEmail().equals(admin.getEmail()) && adminObject.getPassword().equals(admin.getPassword())){
				
				map.put("status", 300);
				return map;
			}
			else{
				map.put("status", 400);
				return map;
			}
			

			
		}catch(Exception e){
			map.put("status",404);
			return map;
		}
		
		
	}
	

	
	@RequestMapping("/addProduct")
	public @ResponseBody Map<String , Object> addProduct(@RequestBody Product product){
		Map<String,Object> map=new HashMap<>();
	    Product prod=new Product();
	    Order orde=new Order();
	    Set<Order> or=new HashSet<>();
	    
	    try{
	    	
	    	
	  categoryObject=categorycrud.findByName(product.getCategory().getName());
	    	
	    
	    offersObject = new Offers();
	    if(product.getOffer() == null){
    	prod.setOffer(null);
    }
    else{
    	offersObject.setDuration(product.getOffer().getDuration());	    
	    offersObject.setOfferCost(product.getOffer().getOfferCost());
    	prod.setOffer(offersObject);
    }
	    
	    
	    if(categoryObject == null){
	    	map.put("status", 300);
	    	return map;
	    }
	    productObject=productCrud.findByName(product.getName());
	    if(productObject != null){
	    	map.put("status",400);
	    	return map;
	    }
	   
        prod.setOrder(or);
	    prod.setCategory(categoryObject);	   
	    prod.setCost(product.getCost());
	    prod.setDescription(product.getDescription());
	    prod.setName(product.getName());
	    prod.setPathImage(product.getPathImage());

	    prod.setOffer(offersObject);
	    productCrud.save(prod);
	    map.put("status","100");
		return map;
	    }
	    
	    catch(Exception e ){
	    	map.put("status",404);
	    	return map;
	    }
	}
	
	
	@RequestMapping("/deleteProduct")
	public @ResponseBody Map<String , Object> deleteProduct(@RequestBody Product product){

		Map<String , Object> map = new HashMap<>();
		productObject=productCrud.findByName(product.getName());
       try{
    	   
    	   if(productObject == null){
   	    	map.put("status", 200);
   	    	return map;
   	       }
		
		   productCrud.delete(productObject.getId());
		   map.put("status", 100);
		  return map;
         }
       catch(Exception e){
    	   map.put("status", 404);
    	   return map;
       }
	}
	
	@RequestMapping("/updateProduct")
	public @ResponseBody Map<String , Object> updateProduct(@RequestBody Product product){
		productObject = productCrud.findByName(product.getName());
		
		Map<String , Object> map = new HashMap<>();
		
		
		  try{
		    	
		    	
			  categoryObject=categorycrud.findByName(product.getCategory().getName());
			    	
			    
			    offersObject = new Offers();
			    if(product.getOffer() == null){
			    	productObject.setOffer(null);
		    }
		    else{
		    	offersObject.setDuration(product.getOffer().getDuration());	    
			    offersObject.setOfferCost(product.getOffer().getOfferCost());
			    productObject.setOffer(offersObject);
		    }
			    
			    
			    if(categoryObject == null){
			    	map.put("status", 300);
			    	return map;
			    }
			    productObject=productCrud.findByName(product.getName());
			    if(productObject == null){
			    	map.put("status",400);
			    	return map;
			    }
			   
		 
			    productObject.setCategory(categoryObject);	   
			    productObject.setCost(product.getCost());
			    productObject.setDescription(product.getDescription());
			    productObject.setName(product.getName());
			    productObject.setPathImage(product.getPathImage());

			    productObject.setOffer(offersObject);
			    productCrud.save(productObject);
			    map.put("status","200");
				return map;
			    }
			    
			    catch(Exception e ){
			    	map.put("status",404);
			    	return map;
			    }

	
		
	}
	
	
	@RequestMapping("/getProductByName")
	public @ResponseBody Map<String , Object> getProductByName(@RequestBody Product product){
		Map<String , Object> map = new HashMap<>();
		productObject=productCrud.findByName(product.getName());
		try{
		if(productObject == null){
			map.put("status", 300);
			return map;
		}
		map.put("name",productObject.getName());
		map.put("cost", productObject.getCost());
		map.put("category",productObject.getCategory().getName());
		map.put("description",productObject.getDescription());
		map.put("offerCost",productObject.getOffer().getOfferCost());
		map.put("offerDuration", productObject.getOffer().getDuration());
		map.put("pathImage",productObject.getPathImage());
		
	
		}
		catch(Exception e){
			map.put("status", 404);
			
		}
		return map;
	}
	
	@RequestMapping("/searchcategory")
	public @ResponseBody Map<String , Object> searchcategory(){
		Map<String , Object> map = new HashMap<>();
		Map<String , Object> map2 = new HashMap<>();
		List<Category> list=new ArrayList<>();
		 
		//map.put("list",list);
		
		
		List<Map<String,Object>> ret=new ArrayList<>();
		list=(List<Category>) categorycrud.findAll();
		for(Category o:list){
			map.put("name", o.getName());
			map.put("description", o.getDescription());
			
			ret.add(map);
		}
		
		map2.put("category", ret);
		return map2;
	}
	
	
	
	@RequestMapping("/searchProduct")
	public @ResponseBody Map<String , Object> searchProduct(){
		Map<String , Object> map = new HashMap<>();
		Map<String , Object> map2 = new HashMap<>();
		List<Product> list=new ArrayList<>();
		 productCrud.findAll().forEach(list::add);
		//map.put("list",list);
		
		
		List<Map<String,Object>> ret=new ArrayList<>();
		
		for(Product o:list){
			map.put("name", o.getName());
			map.put("description", o.getDescription());
			map.put("pathImage", o.getPathImage());
			map.put("categoryName",o.getCategory().getName());
			map.put("offerCost",o.getOffer().getOfferCost());
			
			ret.add(map);
		}
		
		map2.put("products", ret);
		return map2;
	}
	
	@RequestMapping("/getAllProducts")
	public @ResponseBody Map<String, Object> getProduct(){
		Map<String, Object> map = new HashMap<>();
		map.put("products",adminService.getAllProduct()); 
		return map;
	}
	
	@RequestMapping("/getCustomer")
	public @ResponseBody Map<String, Object>  getCustomer(){
		Map<String, Object> map = new HashMap<>();
		map.put("customers",adminService.getAllCustomer());
		return map;
	} 
//	@RequestMapping("/addOffer")
//	public @ResponseBody Map<String , Object> addOffer(@RequestBody Offers offer){
//		Map<String , Object> map = new HashMap<>();
//		
//		if(Integer.valueOf(offer.getDuration()) == null ){
//			map.put("status", 100);
//			return map;
//		}
//		
//		Offers offers=new Offers();
//		offers.setOfferCost(offer.getOfferCost());
//		offers.setDuration(offer.getDuration());
//		offercrud.save(offers);
//		map.put("status",200);
//		return map;
//		
//	}
//	
//	@RequestMapping("/deleteOffer")
//	public @ResponseBody Map<String , Object> deleteOffer(Offers offer){
//		Map<String , Object> map = new HashMap<>();
//		if(Integer.valueOf(offer.getDuration()) == null ){
//			map.put("status", 100);
//			return map;
//		}
//		
//		offercrud.delete(2);
//		
//		return map;
//		
//	}
	
	

	
	@RequestMapping("/addCategory")
	public @ResponseBody Map<String , Object> addCategory(@RequestBody Category category){
		
		Map<String , Object> map = new HashMap<>();
		try{
			categoryObject = categorycrud.findByName(category.getName());
			if(categoryObject == null){
				categoryObject = new Category();
				categoryObject.setName(category.getName());
				categoryObject.setDescription(category.getDescription());
				categorycrud.save(categoryObject);
						
			}
			else{
				map.put("status",200);
				return map;
			}
		}
		catch(Exception e){
			map.put("status", 404);
		}
		map.put("status", 100);
		return map;
	}
	

	@RequestMapping("/updateCategory")
	public @ResponseBody Map<String , Object> updateCategory(@RequestBody Category category){
		
		Map<String , Object> map = new HashMap<>();
		try{
			categoryObject = categorycrud.findByName(category.getName());
			if(categoryObject != null){
			
				categoryObject.setName(category.getName());
				categoryObject.setDescription(category.getDescription());
				categorycrud.save(categoryObject);
						
			}
			else{
				map.put("status",200);
				return map;
			}
		}
		catch(Exception e){
			map.put("status", 404);
		}
		map.put("status", 100);
		return map;
	}
	
	
	@RequestMapping("/deleteCategory")
	public @ResponseBody Map<String , Object> deleteCategory(@RequestBody Category category){
		
		Map<String , Object> map = new HashMap<>();
		try{
			categoryObject = categorycrud.findByName(category.getName());
			if(categoryObject != null){
			
				categorycrud.delete(categoryObject);
						
			}
			else{
				map.put("status",200);
				return map;
			}
		}
		catch(Exception e){
			map.put("status", 404);
		}
		map.put("status", 100);
		return map;
	}
	
	
	@RequestMapping("/getCategory")
	public @ResponseBody Map<String , Object> getCategory(@RequestBody Category category){
		Map<String , Object> map = new HashMap<>();
		categoryObject = categorycrud.findByName(category.getName());
		try{
			if(categoryObject == null){
				map.put("status",200);
			}
		map.put("name", category.getName());
		map.put("description", category.getDescription());
		//map.put("products",category.getProduct());
		}
		catch(Exception e){
			map.put("status", 404);
			return map;
		}
		return map;
		
	}
	
	
}
