package com.example.model;
import org.springframework.data.repository.CrudRepository;
public interface  customerCrud extends CrudRepository<Customer,Integer>{
	
	Customer findByEmail(String email);
	Customer findByPhone(int phone);
	//Order findByOrder(int customerId);

}
