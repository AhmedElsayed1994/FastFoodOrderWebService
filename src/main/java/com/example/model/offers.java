package com.example.model;
import javax.persistence.*;

@Entity
@Table(name = "offers")
public class Offers {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int duration;
	
//	@OneToOne(mappedBy = "offers")
//	private product product;
	
	private Double offerCost; 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

//	public product getProduct() {
//		return product;
//	}
//
//	public void setProduct(product product) {
//		this.product = product;
//	}

	public double getOfferCost() {
		return offerCost;
	}

	public void setOfferCost(double offerCost) {
		this.offerCost = offerCost;
	}
	

}
