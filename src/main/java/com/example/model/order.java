package com.example.model;

import java.sql.Date;
import java.sql.Time;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="order1")
public class Order {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	
	private double totalCost;
	
	//@CreationTimestamp
	//private Date createdAt=new Date();
	
	
	
	@ManyToMany(mappedBy = "order")
	private Set<Product> product;
	
	private Date date;
	private Time time;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public int getId() {
		return id;
	}

	public Set<Product> getProduct() {
		return product;
	}

	public void setProduct(Set<Product> product) {
		this.product = product;
	}

//	public int getId() {
//		return id;
//	}

	public void setId(int id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	
	
	
	
	
	
	

}
