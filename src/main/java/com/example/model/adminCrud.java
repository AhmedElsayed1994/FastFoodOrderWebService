package com.example.model;

import org.springframework.data.repository.CrudRepository;

public interface adminCrud extends CrudRepository<Admin, Integer> {
         Admin findByEmail(String email);
}
