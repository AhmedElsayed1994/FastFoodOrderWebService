package com.example.model;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="category")
public class Category {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	int id;
	
	String name;
	
	String description;
	
//
//	@OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    private Set<Product> product;
	
	
//	public Set<Product> getProduct() {
//		return product;
//	}
//	public void setProduct(Set<Product> product) {
//		this.product = product;
//	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void Category(){}
	
	
}
