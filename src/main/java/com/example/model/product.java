package com.example.model;
import java.util.Set;

import javax.persistence.*;
@Entity
@Table(name="product")
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String name;
	private String description;
	private String pathImage;
	private double cost;
	
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "category_Id")
    private Category category;
    
	
	 @ManyToMany(cascade = CascadeType.ALL)
	    @JoinTable(name = "product_order", joinColumns = @JoinColumn(name = "product_id",
		referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(name = "order_id",
	    referencedColumnName = "id"))
	    private Set<Order> order;
	    
	    
	 @OneToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL  )
	 @JoinColumn(name = "offers_id")
	 private Offers offer;

	
	
	public Offers getOffer() {
		return offer;
	}

	public void setOffer(Offers offer) {
		this.offer = offer;
	}



	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPathImage() {
		return pathImage;
	}

	public void setPathImage(String pathImage) {
		this.pathImage = pathImage;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public Set<Order> getOrder() {
		return order;
	}

	public void setOrder(Set<Order> order) {
		this.order = order;
	}
	
}
