package com.example.model;

import org.springframework.data.repository.CrudRepository;

public interface categoryCrud extends CrudRepository<Category,Integer>{
    Category findByName(String category);
}
