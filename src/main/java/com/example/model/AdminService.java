package com.example.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

	@Autowired
	private customerCrud customerCrud;
	
	public List<Customer> getAllCustomer(){
		List<Customer>list = new ArrayList<>();
	customerCrud.findAll()
	.forEach(list::add);
	return list;
	}
	
	@Autowired
	productCrud productCrud;
	
	public List<Product> getAllProduct(){
		List<Product>list = new ArrayList<>();
	    productCrud.findAll()
	    .forEach(list::add);
	return list;
	}
	
}
